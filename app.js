const cors = require('cors');
const YAML = require('yamljs')
const express = require('express');
const passport = require('passport');
const {PORT} = require('./utils/config');
const bodyParser = require('body-parser');
const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');
const { connectDb } = require('./utils/connection');
const swaggerDocument = YAML.load('./swagger.yaml');


// initialize the express app
const app = express();

// Initialize the middlewares ...
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

connectDb();


// Initialize passport
app.use(passport.initialize());
app.use(passport.session());
require('./utils/passport')(passport);

// Initialize the routes ....
app.use('/api',require('./routes/base'));
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));


// Start the server ....
app.listen(PORT,()=>console.log(`Listening on port http://localhost:${PORT}`));