const Cart = require('../schema/Cart');

const listItems = async (req,res)=>{
	try{
		let id= req.params.id;
		let cartItems = await Cart.find({user_id:id});
		return res.status(200).json({
			error:false,
			list:cartItems
		})

	}catch(err){
		return res.status(403).json({
			error:true,
			message:'Cart Items not fetched'
		})
	}
}

const addItem = async (req,res)=>{
	try{
		let item = new Cart({
			prod_id:req.body.prod_id,
			name:req.body.name,
			price:req.body.price,
			user_id:req.params.id
		})

		//check if the item exists in the cart
		let isItem = await Cart.findOne({prod_id:item.prod_id});

		if(isItem) throw new Error('AE');

		//save the item in the database
		let cartItem = await item.save();

		if(!cartItem) throw new Error('NS');

		return res.status(201).json({
			message:"New item added"
		});


	}catch(err){
		if(err.message === 'NS'){
			return res.status(409).json({
				message:"Item added not possible"
			})
		}
		if(err.message === 'AE'){
			return res.status(403).json({
				message:"Item already added in the cart"
			})
		}
	}
}


const editItem = async(req,res)=>{
	try{
		let uid = req.params.uid;
		let id = req.body.prod_id;
		let increaseOrdecrease = req.body.increaseOrdecrease;
		if(increaseOrdecrease === "true"){
			//increase item quantity
			let increasedResult = await Cart.findOneAndUpdate({prod_id:id,user_id:uid},{$inc:{quantity:1}},{new:true});
			// let increasedResult = await Cart.findOne({prod_id:id})
			// return res.json(increasedResult)
			return res.status(200).json({
				error:false,
				item:increasedResult
			})
		}if(increaseOrdecrease === "false"){
			//decrease item quantity
			let item = await Cart.findOne({prod_id:id});
			if(item.quantity>1){
				let decreasedQuantity = await Cart.findOneAndUpdate({prod_id:id,user_id:uid},{$inc:{quantity:-1}},{new:true});
					return res.status(200).json({
						error:false,
						item:decreasedQuantity
				})
			}else{
				console.log('delete the item from the cart');
				let item = await Cart.findOneAndDelete({prod_id:id});
				return res.status(200).json({
					error:false,
					deleted:item
				})
			}
			
		}
	}catch(err){
		console.log(err.message)
		return res.status(400).json({
			error:true,
			message:"Error occured"
		})

	}
}

module.exports = {
	listItems,
	addItem,
	editItem
}