const User = require('../schema/User');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const register = async (req,res)=>{
	try{
		let user = new User({
			email:req.body.email,
			password:req.body.password
		})

		// return res.json(user);

		//check if the user exists
		let ifExisiting = await User.findOne({email:user.email});

		if(ifExisiting) throw new Error('UR')
		else{
			//Hash the password
			let salt = bcrypt.genSaltSync(10);
			let hash = bcrypt.hashSync(user.password,salt);
			//update the user password
			user.password = hash;

			//Register the user 
			let savedResult = await user.save();
			if(!savedResult) throw new Error('NR');
			return res.status(201).json({
				message:"New User Registered",
				email:savedResult.email
			})

		}
	}catch(err){
		if(err.message === 'UR'){
			return res.status(409).json({
				message:"User Already Registered"
			})
		}
		if(err.message === 'NR'){
			return res.status(403).json({
				message:"Registration failed",
			})
		}
	}
}

const login = async (req,res)=>{
	try{
		let user= {
			email:req.body.email,
			password:req.body.password
		}
		// console.log(user);
		
		//check is the user exists or not
		let ifUser = await User.findOne({email:user.email});
		// console.log(ifUser);
		
		if(!ifUser) throw new Error('NX')
		//compare the user password with the saved password
		let comparePassword = bcrypt.compareSync(user.password,ifUser.password)
		if(!comparePassword) throw new Error('WP');

		//Generate TOKEN
		let token = jwt.sign({id:ifUser._id},'secretKey',{ expiresIn: '1h' });
		return res.status(200).json({
			token: `Bearer ${token}`,
			id:ifUser._id
		})

	}catch(err){
		if(err.message === 'NX'){
			return res.status(403).json({
				message:'User\'s email does not exists'
			})
		}

		if(err.message === 'WP'){
			return res.status(403).json({
				message:'Wrong password provided'
			})
		}

	}
}


const addDeliveryDetails = async (req,res)=>{
	try{
		let id = req.params.id;
		let userDetails = {
			name:req.body.name,
			address:req.body.address,
			pincode:req.body.pincode,
			phone:req.body.phone,
			landmark:req.body.landmark
		}

		let savedDetails = await User.findOneAndUpdate({_id:id},userDetails,{new:true});
		return res.json({
			message:"Delivery details saved",
			user:savedDetails
		})

	}catch(err){
		console.log(err.message);
	}
}

module.exports ={
	register,
	login,
	addDeliveryDetails
}