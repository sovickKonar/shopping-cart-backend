const mongoose = require('mongoose');

const {Schema} = mongoose;

const UserSchema = new Schema({
	name:{
		type:String,
	},
	email:{
		type:String,
		required:true
	},
	password:{
		type:String,
		required:true
	},
	pincode:{
		type:Number,
	},
	address:{
		type:String
	},
	phone:{
		type:String
	},
	landmark:{
		type:String
	}
},{timestamps:true});

module.exports = mongoose.model('user',UserSchema);