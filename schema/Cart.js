const mongoose = require('mongoose');
const { Schema } = mongoose;

const CartSchema = new Schema({
	prod_id:{
		type:Number,
		required:true
	}, 
	name:String,
	price:String,
	quantity:{
		type:Number,
		default:1,
	},
	user_id:String,
},{timestamps:true});




module.exports = mongoose.model('cart',CartSchema);