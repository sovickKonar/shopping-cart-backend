
const JwtStrategy = require('passport-jwt').Strategy;
const {ExtractJwt} = require('passport-jwt');
const User = require('../schema/User');

module.exports = (passport)=>{
    passport.use(new JwtStrategy({
        jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('Bearer'),
        secretOrKey:'secretKey'
    },async(payload,done)=>{
        try{
            // find the user's specified in token
            let user = await User.findOne({_id:payload.id});
            if(!user){
                return done(null,false)
            }
            done(null,user)
        }catch(err){
            done(err,false);
        }
    
    }));
}
