const mongoose = require('mongoose');

const { URI } = require('./config');

function connectDb(){
	mongoose.connect(URI,{useNewUrlParser:true, useUnifiedTopology: true,useFindAndModify:false})
	.then(()=>{
		console.log('database connected');
	})
	.catch(er=>{
		console.log('error occured :',err.message);
	})
}

module.exports = {connectDb}