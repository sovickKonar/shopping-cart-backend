const router = require('express').Router();
const passport = require('passport');
const {listItems,addItem,editItem} = require('../controllers/cart');

/**
 * @swagger
 * /api/cart/list-items:
 *   get:
 *     summary: List of all the items in the cart
 *     description: This end point provides either an empty array or array of objects of items stored in the cart
 *     responses:
 *       200:
 *         description: A list of items.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 error:
 *                   type: string
 *                 list:
 *                   type: array
 *                   items:
 *                     type: object
 *                     properties:
 *                       quantity:
 *                         type: integer
 *                         description: The cart quantity.
 *                         example: 1
 *                       _id:
 *                         type: string
 *                         description: The user's id.
 *                         example: 604f3de68a857e30a2e0b0e4
 *                       prod_id:
 *                         type: integer
 *                         description: The product's unique id.
 *                         example: 125
 *                       name:
 *                         type: string
 *                         description: The product's name.
 *                         example: iPhone
 *                       price:
 *                         type: integer
 *                         description: The product's price.
 *                         example: 125000
*/
router.get('/list-items/:id',passport.authenticate('jwt',{session:false}),listItems);

/**
 * @swagger
 * /api/cart/add-item:
 *   post:
 *     summary: Add a new item in the cart
 *     description: This end point adds a new item in the cart
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 description: The product's name.
 *                 example: Dell laptop
 *               price:
 *                 type: integer
 *                 description: The product's price.
 *                 example: 60500
 *               prod_id:
 *                 type: integer
 *                 description: The product's unique id.
 *                 example: 785 
 *     responses:
 *       200:
 *         description: A new item is created.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 message:
 *                   type: string
 *                   description: success string  
 *                   example: new item added 
*/
router.post('/add-item/:id',passport.authenticate('jwt',{session:false}),addItem);

/**
 * @swagger
 * /api/cart/edit-item:
 *   put:
 *     summary: Edit an item in the cart
 *     description: This end point increase or decrease the quantity of an item in the cart
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               prod_id:
 *                 type: integer
 *                 description: The product's unique id.
 *                 example: 785 
 *               increaseOrdecrease:
 *                 type: string
 *                 description: true means increase quantity false means decrease quamtity.
 *                 example: true
 *     responses:
 *       200:
 *         description: An item quantity is increased or decresed.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 error:
 *                   type: string
 *                   description: either error is true or false  
 *                   example: false   
*/
 
router.put('/edit-item/:uid',passport.authenticate('jwt',{session:false}),editItem);

module.exports = router;