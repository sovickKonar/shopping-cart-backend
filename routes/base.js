const router = require('express').Router();
const { base } = require('../controllers/base');

/**
 * @swagger
 * /api:
 *   get:
 *     summary: checks the status of the server
 *     description: This end point checks whether the server has started or not and gives a success message.
*/
router.get('/',base);
router.use('/cart',require('./cart'));
router.use('/user',require('./user'));

module.exports = router;