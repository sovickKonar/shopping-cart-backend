const router = require('express').Router();
const passport = require('passport');
const { register,login,addDeliveryDetails } = require('../controllers/user');


/**
 * @swagger
 * /api/user/register:
 *   post:
 *     summary: User Registration
 *     description : User can enter email and password to create a new account
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
*           schema:
 *             type: object
 *             properties:
 *               email:
 *                 type: string
 *                 description: The user's email.
 *                 example: sovk@gmail.com
 *               password:
 *                 type: string
 *                 description: The user's password.
 *                 example: sovk@123
 *     responses:
 *       200:
 *         description: A new item is created.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 message:
 *                   type: string
 *                   description: success string  
 *                   example: new user registered
 *                 email:
 *                   type: string
 *                   description: user email that got registered  
 *                   example: sovk@gmail.com
 */
router.post('/register',register)

/**
 * @swagger
 * /api/user/login:
 *   post:
 *     summary: User Login
 *     description : User can enter email and password to login into the account
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
*           schema:
 *             type: object
 *             properties:
 *               email:
 *                 type: string
 *                 description: The user's email.
 *                 example: sovk@gmail.com
 *               password:
 *                 type: string
 *                 description: The user's password.
 *                 example: sovk@123
 *     responses:
 *       200:
 *         description: Token and user id is generated.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 token:
 *                   type: string
 *                   description: jwt token is created   
 *                   example: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYwNGYwMDk2MDlhZjViMzY4ZWQ2OTA5ZiIsImlhdCI6MTYxNTc5NTE3NiwiZXhwIjoxNjE1Nzk4Nzc2fQ.4HbyL3ZnLV-6d9EkgNyCfNpjELx1yZ3-4mQMjLdc_Vo
 *                 id:
 *                   type: string
 *                   description: user id of the logged in user  
 *                   example: 604f009609af5b368ed6909f
 */
router.post('/login',login)



/**
 * @swagger
 * /api/user/delivery-details/{id}:
 *   put:
 *     summary: Save the details of the user
 *     description: This end point receives data from the user and update the delivery details for any user
*/
router.put('/delivery-details/:id',passport.authenticate('jwt',{session:false}),addDeliveryDetails)

module.exports = router;

